document.addEventListener("DOMContentLoaded", function(event) {

  function scrollProgressBar() {

  var windowTop = window.scrollY;
  var documentHeight = document.documentElement.scrollHeight;
  var windowHeight = window.innerHeight;

  var position = (windowTop/(documentHeight-windowHeight))*100;

  document.getElementById('progressBarIdForJs').setAttribute('style', 'width: ' + position + '%');
}

window.addEventListener('scroll', scrollProgressBar);
});