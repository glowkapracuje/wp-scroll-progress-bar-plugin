<?php
/**
 * Plugin Name: WP Scroll Progress Bar
 * Plugin URI: https://bitbucket.org/glowkapracuje/wp-scroll-progress-bar-plugin/
 * Description: Lightweight plugin that provides a visual representation of how much of a site remains.
 * Version: 0.1.0
 * Author: Glowkapracuje
 * Author URI: https://bitbucket.org/glowkapracuje/
 */

/*
 * Add style file
 */
add_action( 'wp_enqueue_scripts', 'enqueue_styles_progressBarStyle' );
function enqueue_styles_progressBarStyle()
{
    wp_enqueue_style('progressBarStyle-styles', plugin_dir_url( __FILE__ ) . 'css/progressBarStyle.css', array(), '', 'all' );
}

/*
 * Add js script file
 */
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_progressBarScript' );
function enqueue_scripts_progressBarScript() {
    wp_enqueue_script( 'progressBarScript-scripts', plugin_dir_url( __FILE__ ) . 'js/progressBarScript.js', array( 'jquery' ), '', false );
}

/*
 * Include some html, call css by id and name id for js
 */
add_action('wp_footer', 'scrollBar_html');
function scrollBar_html(){
    ?>
    <div id="ProgressBarContainer">
        <span id="progressBarIdForJs" class="progressBarFill"></span></div>
    <?php
}