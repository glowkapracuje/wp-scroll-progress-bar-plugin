# WP Scroll Progress Bar plugin

Lightweight plugin that provides a visual representation of how much of a site remains.


##Introduction

When a user scrolls down the page, the progress bar begin to fill.


##Preview

![WP Scroll Progress Bar plugin preview](wp-scroll-progress-bar-plugin-preview.gif)


##Technologies

* JavaScript
* PHP
* HTML
* CSS


##Setup

To use this plugin on your wordpress site:

1.  Download the "wp-scroll-progress-bar-plugin" folder.
2.  Navigate to the "wp-content/plugins" directory and upload plugin folder here, via e.g. [FileZilla](https://filezilla-project.org/).
3.  Clear cache in your browser and refresh page site.